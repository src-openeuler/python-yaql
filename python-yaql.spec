%global _empty_manifest_terminate_build 0
Name:		python-yaql
Version:	3.0.0
Release:	1
Summary:	YAQL - Yet Another Query Language
License:	Apache-2.0
URL:		http://yaql.readthedocs.io
Source0:	https://files.pythonhosted.org/packages/source/y/yaql/yaql-%{version}.tar.gz
BuildArch:	noarch

Requires:	python3-Babel
Requires:	python3-pbr
Requires:	python3-ply
Requires:	python3-dateutil
Requires:	python3-six

%description
YAQL (Yet Another Query Language) is an embeddable and extensible query
language, that allows performing complex queries against arbitrary objects. It
has a vast and comprehensive standard library of frequently used querying
functions and can be extend even further with user-specified functions. YAQL is
written in python and is distributed via PyPI.

%package -n python3-yaql
Summary:	YAQL - Yet Another Query Language
Provides:	python-yaql
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires: python3-pbr
%description -n python3-yaql
YAQL (Yet Another Query Language) is an embeddable and extensible query
language, that allows performing complex queries against arbitrary objects. It
has a vast and comprehensive standard library of frequently used querying
functions and can be extend even further with user-specified functions. YAQL is
written in python and is distributed via PyPI.

%package help
Summary:	Development documents and examples for yaql
Provides:	python3-yaql-doc
%description help
YAQL (Yet Another Query Language) is an embeddable and extensible query
language, that allows performing complex queries against arbitrary objects. It
has a vast and comprehensive standard library of frequently used querying
functions and can be extend even further with user-specified functions. YAQL is
written in python and is distributed via PyPI.

%prep
%autosetup -n yaql-%{version} -p1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-yaql -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Aug 26 2024 yaoxin <yao_xin001@hoperun.com> 3.0.0-1
- Update to 3.0.0:
  * Use py3 as the default runtime for tox
  * Update python classifier in setup.cfg
  * Publish release notes
  * Restore Python 3.6/7 support
  * Replace assertItemsEqual with assertCountEqual
  * [community goal] Update contributor documentation
  * Stop using deprecated 'message' attribute in Exception
  * Drop direct execution of run.py
  * Remove format function
  * Update CI to use unversioned jobs template
  * Bump hacking
  * Migrate from testr to stestr
  * Remove translation sections from setup.cfg
  * Remove shebang from setup.py
  * setup.cfg: Replace dashes by underscores

* Tue Mar 19 2024 wangkai <13474090681@163.com> - 2.0.0-2
- Fix CVE-2024-29156

* Wed Jul 27 2022 liksh <liks11@chinaunicom.cn> - 2.0.0-1
- Upgrade for openstack yoga

* Wed May 11 2022 xigaoxinyan <xigaoxinyan@h-partners.com> - 1.1.3-2
- License compliance rectification

* Mon Aug 24 2020 Python_Bot <Python_Bot@openeuler.org> - 1.1.3-1
- Package Spec generated
